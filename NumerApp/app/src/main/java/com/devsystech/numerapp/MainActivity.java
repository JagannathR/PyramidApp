package com.devsystech.numerapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.*;


public class MainActivity extends AppCompatActivity {

    public String tempr = "";
    public int count = 0;
    RelativeLayout rel_layout;
    EditText number_et1;
    Button bt1;
    TextView input_total, print_array, id_input;
    String inputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.devsys));

        rel_layout = (RelativeLayout) findViewById(R.id.relative_layout);
        number_et1 = (EditText) findViewById(R.id.id_input_et);
        bt1 = (Button) findViewById(R.id.id_input_btn);
        id_input = (TextView) findViewById(R.id.id_input);
        input_total = (TextView) findViewById(R.id.id_input_total);
        print_array = (TextView) findViewById(R.id.id_array);


        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            resetPyramid();

            try
            {
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
            catch (Exception e)
            {

            }

            inputData = number_et1.getText().toString();
            inputData = inputData.toLowerCase();
            inputData = inputData.replaceAll("\\s", "");
            inputData = inputData.replaceAll("-", "");
            id_input.setText("Input:  " + inputData);
            count = inputData.length();
            calculate(inputData);
            number_et1.setText("");
            }
        });
    }


    /* A Function to get values for each character in the Input string*/
    public void calculate(String input) {
        int total = 0;
        for (int i = 0; i < input.length(); i++) {
            total += getValue(input.charAt(i));
        }
        int formatted_total = total;
        while (formatted_total > 9) {
            formatted_total = recurCompute(formatted_total);
        }

        input_total.setText("Total:  " + "(" + total + ") " + formatted_total);
        int[] a = new int[input.length()];
        int aj = 0;
        String temp = "";
        for (int j = 0; j < input.length(); j++) {
            temp += (char) input.charAt(j);
            temp += "    ";
            int m = getValue(input.charAt(j));
            if (m >= 0) {
                a[aj] = m;
                aj++;
            }
        }
        int n = 0;
        while (a.length > 0) {
            display(a, n);
            a = compute(a);
            n++;
        }
        display(a, n);

    }


    /* A Function to reset the Views of the Layout*/
    void resetPyramid() {
        id_input.setText("");
        input_total.setText("");
        print_array.setText("");
        tempr = "";
    }


    /* A Function to get the input array and pass the elements whose sum has to be computed  */
    int[] compute(int[] b) {
        int val;
        int j;
        int[] a2 = new int[b.length - 1];

        for (j = b.length - 1; j > 0; j--) {
            val = recurCompute(b[j] + b[j - 1]);
            a2[j - 1] = val;
        }
        String tt = "";
        for (int k = 0; k < a2.length; k++) {
            int a = a2[k];
            tt += a;
            tt += " ";
        }
        return a2;
    }


    /* A Recursive Function to receive the elements and return their sum as a single digit */
    int recurCompute(int x) {
        int value = 0;
        if (x > 9) {
            value = recurCompute(x / 10);
        }
        value = value + x % 10;
        if (value > 9) {
            value = recurCompute(value / 10);
        }
        return value;
    }


    /*  A Function to display the values of the string and their corresponding sums */
    void display(int[] print, int linenumber) {
        for (int i = 0; i < linenumber; i++)
            tempr += "";

        for (int k = 0; k < print.length; k++) {
            int a = print[k];
            tempr += String.valueOf(a);
            tempr += "   ";

        }
        count--;
        tempr += "\n";

        if (count >= 0) {
            print_array.setText(tempr);
            //print_array.setMovementMethod(new ScrollingMovementMethod());
        }

    }


    /* A Function to get the stored values of the inputed string */
    int getValue(char c) {
        HashMap<Character, Integer> hm1 = new HashMap<Character, Integer>();
        int a;
        char x = '0';

        hm1.put('a', 1);
        hm1.put('b', 2);
        hm1.put('c', 3);
        hm1.put('d', 4);
        hm1.put('e', 5);
        hm1.put('f', 8);
        hm1.put('g', 3);
        hm1.put('h', 5);
        hm1.put('i', 1);
        hm1.put('j', 1);
        hm1.put('k', 2);
        hm1.put('l', 3);
        hm1.put('m', 4);
        hm1.put('n', 5);
        hm1.put('o', 7);
        hm1.put('p', 8);
        hm1.put('q', 1);
        hm1.put('r', 2);
        hm1.put('s', 3);
        hm1.put('t', 4);
        hm1.put('u', 6);
        hm1.put('v', 6);
        hm1.put('w', 6);
        hm1.put('x', 5);
        hm1.put('y', 1);
        hm1.put('z', 7);

        for (int j = 0; j <= 9; j++) {
            hm1.put(x, j);
            x++;
        }

        if (hm1.containsKey(c)) {
            a = hm1.get(c);
        } else {
            a = -1;
        }
        return a;
    }

}


